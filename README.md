This project is dual licensed under CC BY-SA 4.0 and MIT.

For more information about the game and its controlls/setup, see the [projects itch.io page](https://flupppi.itch.io/junkyard-jousters)
