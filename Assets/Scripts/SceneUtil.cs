using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public static class SceneUtil {
	/// <summary>
	/// Helper function to load a different scene, keeping some GameObjects alive.
	/// </summary>
	/// <param name="scene">The next scene to load. <see href="https://github.com/dbrizov/NaughtyAttributes#scene">Useful utility</see></param>
	/// <param name="keep">List of GameObjects to transfer to the next scene (will become top-level GameObjects)</param>
	/// <param name="onDone">A callback that will be called once the new scene is fully loaded, and that receives the keep-list as a parameter</param>
	public static void LoadScene(string scene, List<GameObject> keep, Action<List<GameObject>> onDone) {
		var go = new GameObject("Scene Changer");
		Object.DontDestroyOnLoad(go);
		foreach (var g in keep) {
			g.transform.SetParent(go.transform, true);
		}

		var helper = go.AddComponent<SceneLoadHelper>();
		helper.OnDone    = onDone;
		helper.sceneName = scene;
	}
}

public class SceneLoadHelper : MonoBehaviour {
	public Action<List<GameObject>> OnDone;
	public string                   sceneName;

	private void Start() {
		StartCoroutine(LoadYourAsyncScene());
	}
	
	IEnumerator LoadYourAsyncScene() {
		yield return new WaitForSecondsRealtime(0.5f);
		
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
		while (!asyncLoad.isDone)
			yield return null;

		SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByName(sceneName));
		
		var keep = new List<GameObject>();
		for (int i = 0; i < transform.childCount; ++i)
			keep.Add(transform.GetChild(i).gameObject);

		foreach (var c in keep) {
			c.transform.parent = null;
		}

		OnDone(keep);

		Destroy(gameObject);
	}
	
}
