using UnityEngine;

public class Score : MonoBehaviour {
	public static Score Instance { get; private set; }

	private int[] scores = new int[2];

	private void Awake() {
		if(Instance)
			Debug.LogError("Multiple Instances of BeatManager in the current scene!");
		Instance = this;
	}
	private void OnDestroy() {
		if(Instance!=this)
			Debug.LogError("Multiple Instances of BeatManager in the current scene!");
		else
			Instance = null;
	}

	public void IncrementScore(Team team) {
		if((int) team > 0 && (int) team < scores.Length)
			scores[(int) team]++;
	}

	public void IncrementOtherTeamScores(Team team) {
		for (int i = 0; i < scores.Length; ++i)
			if (i != (int) team)
				scores[i]++;
	}

	public int GetScore(int team) {
		return scores[team];
	}
}
