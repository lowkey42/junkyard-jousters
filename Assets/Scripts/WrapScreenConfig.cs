using System;
using UnityEngine;

public class WrapScreenConfig : MonoBehaviour
{
	public float border = 38f;

	public Camera Camera { get; private set; }
	
	public static WrapScreenConfig Instance { get; private set; }

	private void Awake()
	{
		if (Instance)
			Debug.LogError("Multiple Instances of WrapScreenConfig in the current scene!");

		Instance = this;
		Camera   = GetComponent<Camera>();
		if(!Camera)
			Debug.LogError("WrapScreenConfig without Camera component!");
	}
}