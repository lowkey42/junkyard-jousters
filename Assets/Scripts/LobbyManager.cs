using System;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class LobbyManager : MonoBehaviour {
	[SerializeField]        private TMP_Text         text;
	[SerializeField]        private TMP_Text         tutorialMessage;
	[SerializeField, Scene] private String           sceneName;
	[SerializeField]        private List<GameObject> playerPrefabs;

	[SerializeField] private List<AudioClip> music = new();
	[SerializeField] private TMP_Text        trackName;

	[ShowNonSerializedField]
	private List<GameObject>                   availablePlayerPrefabs;
	private Dictionary<GameObject, GameObject> playerInstanceToPrefab = new();

	private List<ExternalMusic> tracks;
	private int                 selectedTrack;
	
	private List<GameObject> players     = new();
	public  float            timerLength = 5.0f;
	private float            currentTime;
	private int              playerCount  = 0;
	private bool             gameStarting = false;

	public void NextTrack() {
		selectedTrack = (selectedTrack + 1) % tracks.Count;
		
		BeatManager.Instance.Play(tracks[selectedTrack].clip, tracks[selectedTrack].beats);
		if (trackName)
			trackName.text = tracks[selectedTrack].title;
	}
	public void PreviousTrack() {
		selectedTrack--;
		if (selectedTrack < 0)
			selectedTrack = tracks.Count - 1;
		
		BeatManager.Instance.Play(tracks[selectedTrack].clip, tracks[selectedTrack].beats);
		if (trackName)
			trackName.text = tracks[selectedTrack].title;
	}

	private void Start() {
		availablePlayerPrefabs = new List<GameObject>(playerPrefabs);

		tracks        = MusicFSLoader.GetTrackList(music);
		selectedTrack = tracks.Count - 1;

		if (tracks.Count > 1) {
			trackName.gameObject.transform.parent.gameObject.SetActive(true);
			BeatManager.Instance.Play(tracks[selectedTrack].clip, tracks[selectedTrack].beats);
			if (trackName)
				trackName.text = tracks[selectedTrack].title;
		}
	}

	public GameObject AddPlayer() {
		if (availablePlayerPrefabs.Count == 0)
			return null;

		var prefabIndex = Random.Range(0, availablePlayerPrefabs.Count);
		var prefab      = availablePlayerPrefabs[prefabIndex];
		availablePlayerPrefabs.RemoveAt(prefabIndex);

		var p = Instantiate(prefab);
		
		playerInstanceToPrefab.Add(p, prefab);

		playerCount++;
		p.GetComponentInChildren<TMP_Text>().text = $"P{playerCount}";
		players.Add(p);
		return p;
	}

	public void RemovePlayer(GameObject p) {
		var prefab = playerInstanceToPrefab[p];
		playerInstanceToPrefab.Remove(p);
		availablePlayerPrefabs.Add(prefab);
		
		players.Remove(p);
	}

	private bool IsGameReady() {
		if (players.Count == 0) {
			return false;
		}

		foreach (GameObject p in players) {
			if (!p.GetComponent<TeamMember>().HasTeam()) {
				return false;
			}
		}

		return true;
	}

	private List<GameObject> FindControllers() {
		List<GameObject> controllers = new();
		foreach (GameObject p in players) {
			controllers.Add(p.transform.parent.parent.GameObject());
		}

		return controllers;
	}

	private void Update() {
		if (gameStarting) {
			text.text               = $"Loading";
			text.enabled            = true;
			tutorialMessage.enabled = false;
			return;
		}

		if (IsGameReady()) {
			currentTime -= Time.unscaledDeltaTime;
			var seconds = ((int) currentTime) % 60;
			text.text               = $"Game Starts in {seconds:0}";
			text.enabled            = true;
			tutorialMessage.enabled = false;
			if (currentTime <= 0.0f) {
				gameStarting            = true;
				MusicFSLoader.MainTrack = tracks[selectedTrack];
				CameraFade.Instance.FadeOut();
				SceneUtil.LoadScene(sceneName, FindControllers(), OnLevelLoaded);
			}
		} else {
			currentTime             = timerLength;
			text.enabled            = false;
			tutorialMessage.enabled = true;
		}
	}

	private static void OnLevelLoaded(List<GameObject> controllers) {
		foreach (var c in controllers) {
			var join = c.GetComponentInChildren<PlayerJoin>();
			if (!join)
				continue;
			
			join.inLobby = false;

			foreach (var p in c.GetComponentsInChildren<TeamMember>()) {
				switch (p.GetTeam()) {
					case Team.None:
						break;
					case Team.Red:
						p.transform
						 .SetPositionAndRotation(new Vector3(UnityEngine.Random.Range(-16, -22), UnityEngine.Random.Range(20, 23)),
						                         Quaternion.identity);
						break;
					case Team.Blue:
						p.transform
						 .SetPositionAndRotation(new Vector3(UnityEngine.Random.Range(15, 20), UnityEngine.Random.Range(20, 23)),
						                         Quaternion.identity);
						break;
				}
			}
		}
	}
}
