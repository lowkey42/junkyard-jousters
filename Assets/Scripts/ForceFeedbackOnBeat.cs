using UnityEngine;

[RequireComponent(typeof(ForceFeedback), typeof(PlayerJoin))]
public class ForceFeedbackOnBeat : BeatAwareMonoBehaviour {
	[SerializeField] private float beatFeedbackLowFreq         = 0.1f;
	[SerializeField] private float beatFeedbackHighFreq        = 0.2f;
	[SerializeField] private float beatFeedbackDuration        = 0.1f;
	[SerializeField] private float beatFeedbackDurationPercent = 0.5f;

	private ForceFeedback forceFeedback;
	private PlayerJoin    join;

	private void Awake() {
		forceFeedback = GetComponentInParent<ForceFeedback>();
		join          = GetComponentInParent<PlayerJoin>();
	}

	protected override void OnBeat(float timeToNextBeat) {
		if (join.playerCount == 0) return;

		forceFeedback.Rumble(beatFeedbackLowFreq, beatFeedbackHighFreq, Mathf.Min(beatFeedbackDuration, timeToNextBeat * beatFeedbackDurationPercent), true);
	}
}
