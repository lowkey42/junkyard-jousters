using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public delegate void GameStart();

public delegate void GameOver();

public delegate void Beat(float timeToNextBeat);

public class BeatManager : MonoBehaviour {
	public static BeatManager Instance { get; private set; }

	public static event Beat      OnBeat      = delegate(float beat) { };
	public static event GameStart OnGameStart = delegate { };
	public static event GameOver  OnGameOver  = delegate { };

	[SerializeField] private AudioClip   music;
	[SerializeField] private float       beatLength         = 0.2f;
	[SerializeField] private float       nextBeatFadeLength = 0.1f;
	[SerializeField] public  AudioSource audioSource;

	[SerializeField] private float minTimeScale = 0.01f;
	[SerializeField] private float maxTimeScale = 4f;

	private List<float> beats;
	private int         nextBeatIndex  = 0;
	private float       timeSinceBeat  = 0;
	private float       timeToNextBeat = 0;
	private bool        gameRunning    = false;

	public float TimeSinceBeat  => timeSinceBeat;
	public float TimeToNextBeat => timeToNextBeat;

	public void Play(AudioClip clip, List<float> beatsIn = null) {
		music            = clip;
		beats            = beatsIn != null ? ProcessBeats(beatsIn, beatLength) : LoadCsvByName(clip.name, beatLength);
		audioSource.clip = clip;
		audioSource.time = 0;
		audioSource.Stop();
		audioSource.Play();
		gameRunning = true;
		OnGameStart();
	}

	private void Awake() {
		if (Instance)
			Debug.LogError("Multiple Instances of BeatManager in the current scene!");

		Instance = this;
	}

	private void OnDestroy() {
		if (Instance != this)
			Debug.LogError("Multiple Instances of BeatManager in the current scene!");
		else
			Instance = null;
	}

	private void Start() {
		audioSource.Stop();
		Time.timeScale = 0;

		audioSource = GetComponent<AudioSource>();

		if (MusicFSLoader.MainTrack!=null) {
			StartCoroutine(PlayInSeconds(1, MusicFSLoader.MainTrack.clip, MusicFSLoader.MainTrack.beats));
		} else if (music)
			StartCoroutine(PlayInSeconds(1, music));
	}

	private IEnumerator PlayInSeconds(float seconds, AudioClip clip, List<float> beatsIn = null) {
		yield return new WaitForSecondsRealtime(seconds);
		Play(clip, beatsIn);
	}

	private void Update() {
		if (beats == null || !audioSource.isPlaying) {
			UpdateAfterGame();
			return;
		}

		var t    = audioSource.time;
		var beat = false;

		if (nextBeatIndex > 0 && beats[nextBeatIndex - 1] > t) {
			Debug.Log("Reset nextBeatIndex");
			nextBeatIndex = 0;
		}

		while (nextBeatIndex < beats.Count && beats[nextBeatIndex] <= t) {
			nextBeatIndex++;
			beat = true;
		}

		if (nextBeatIndex >= beats.Count || nextBeatIndex == 0) {
			UpdateAfterGame();
			return;
		}

		timeSinceBeat  = t                    - beats[nextBeatIndex - 1];
		timeToNextBeat = beats[nextBeatIndex] - t;

		if (beat) {
			OnBeat(timeToNextBeat);
		}

		var fadeIn  = Mathf.SmoothStep(maxTimeScale, minTimeScale, timeToNextBeat / nextBeatFadeLength);
		var fadeOut = Mathf.SmoothStep(maxTimeScale, minTimeScale, timeSinceBeat  / beatLength);
		Time.timeScale = Mathf.Max(fadeIn, fadeOut);
	}

	private void UpdateAfterGame() {
		if (gameRunning && nextBeatIndex > 0) {
			gameRunning = false;
			Debug.Log("GameOver");
			OnGameOver();
		}

		Time.timeScale = nextBeatIndex > 0 ? Mathf.MoveTowards(Time.timeScale, 1f, 0.1f * Time.unscaledDeltaTime) : 0f;
	}


	public static List<float> LoadCsvByName(string name, float minBeatLength) {
		return LoadCsv(Resources.Load<TextAsset>(name), minBeatLength);
	}

	public static List<float> LoadCsv(TextAsset data, float minBeatLength) {
		if(!data)
			return new List<float>();
		
		return LoadCsv(data.text, minBeatLength);
	}

	public static List<float> LoadCsv(string data, float minBeatLength) {
		var beats = new List<float>();

		var lines = data.Split('\n');
		foreach (var line in lines) {
			var space = line.IndexOf('\t');
			if (space < 0) continue;
			var t = float.Parse(line.Substring(0, space), CultureInfo.InvariantCulture);

			if (beats.Count == 0 || beats.Last() + minBeatLength < t)
				beats.Add(t);
		}

		Debug.Log("Loaded " + beats.Count + " beats (" + lines.Length + " lines)");

		return beats;
	}

	private static List<float> ProcessBeats(List<float> beatsIn, float minBeatLength) {
		var beats = new List<float>();

		foreach (var beat in beatsIn) {
			if (beats.Count == 0 || beats.Last() + minBeatLength < beat)
				beats.Add(beat);
		}

		return beats;
	}
}
