using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Haptics;

[RequireComponent(typeof(PlayerInput))]
public class ForceFeedback : MonoBehaviour {
	private class RumbleReq {
		public float lowFrequencyStrength;
		public float highFrequencyStrength;
		public float duration;
		public bool  fadeOut;
		public float initialDuration;
	}

	private          PlayerInput     playerInput;
	private readonly List<RumbleReq> rumbleRequests = new();

	private void Awake() {
		playerInput = GetComponent<PlayerInput>();
	}

	public void Rumble(float lowFrequencyStrength = 0f, float highFrequencyStrength = 0f, float duration = 0f, bool fadeOut = false) {
		rumbleRequests.Add(new RumbleReq() {
			                                   lowFrequencyStrength = lowFrequencyStrength, highFrequencyStrength = highFrequencyStrength, duration = duration,
			                                   fadeOut              = fadeOut, initialDuration                    = duration
		                                   });
	}

	private void Update() {
		var lowFrequencyStrength  = 0f;
		var highFrequencyStrength = 0f;

		foreach (var r in rumbleRequests) {
			var alpha = r.fadeOut ? Mathf.SmoothStep(0f, 1f, 1f - r.duration / r.initialDuration) : 1f;
			lowFrequencyStrength  = Mathf.Max(lowFrequencyStrength,  r.lowFrequencyStrength  * alpha);
			highFrequencyStrength = Mathf.Max(highFrequencyStrength, r.highFrequencyStrength * alpha);
		}

		foreach (var d in playerInput.devices) {
			if (d is IDualMotorRumble rumble) {
				rumble.SetMotorSpeeds(lowFrequencyStrength, highFrequencyStrength);
			}
		}

		rumbleRequests.RemoveAll(r => {
			                         r.duration -= Time.unscaledDeltaTime;
			                         return r.duration <= 0;
		                         });
	}
}
