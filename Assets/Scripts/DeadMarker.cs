using DefaultNamespace;
using TMPro;
using UnityEngine;

public class DeadMarker : MonoBehaviour {
	public CountDownTimer timer;
	public TeamMember     teamMember;
	public TMP_Text       playerLabel;

	private void OnValidate() {
		timer       = GetComponentInChildren<CountDownTimer>();
		teamMember  = GetComponentInChildren<TeamMember>();
		playerLabel = GetComponentInChildren<TMP_Text>();
	}

	private void Awake() {
		if(!timer)
			timer       = GetComponentInChildren<CountDownTimer>();
		
		if(!teamMember)
			teamMember  = GetComponentInChildren<TeamMember>();
		
		if(!playerLabel)
			playerLabel = GetComponentInChildren<TMP_Text>();
	}
}
