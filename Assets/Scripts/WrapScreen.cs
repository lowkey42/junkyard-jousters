using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


public class WrapScreen : MonoBehaviour
{

    private float  screenHalfWidthInUnits;
    
    void Update()
    {
        var config = WrapScreenConfig.Instance;
        if (!config)
            return;
        var cam = config.Camera;
        screenHalfWidthInUnits = cam.orthographicSize * cam.aspect;
        var pos = transform.position;
        if(pos.x > screenHalfWidthInUnits || pos.x > config.border)
        {
            transform.position= new Vector3(-pos.x + 0.5f, pos.y);
        }
        if(pos.x < -screenHalfWidthInUnits || pos.x < -config.border)
        {
            transform.position = new Vector3(-pos.x - 0.5f, pos.y);
        }
    }

}
