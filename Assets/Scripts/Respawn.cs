using System.Collections;
using NaughtyAttributes;
using TMPro;
using UnityEngine;


public class Respawn : MonoBehaviour {
	public float respawnDelay = 5f;

	[SerializeField, ShowAssetPreview] private GameObject deadMarkerPrefab;

	[SerializeField] private TeamMember teamMember;
	[SerializeField] private TMP_Text   playerLabel;

	[SerializeField] private float      deadFeedbackLowFreq  = 0.8f;
	[SerializeField] private float      deadFeedbackHighFreq = 0.5f;
	[SerializeField] private float      deadFeedbackDuration = 0.5f;
	
	private                  Team       lastTeam             = Team.None;
	private                  GameObject cachedDeadMarkerGo   = null;
	private                  DeadMarker cachedDeadMarker     = null;

	private ForceFeedback forceFeedback;
	
	private void OnValidate() {
		teamMember    = GetComponentInChildren<TeamMember>();
		playerLabel   = GetComponentInChildren<TMP_Text>();
	}

	public void AssignController(ControllerAssignment a) {
		forceFeedback = GetComponentInParent<ForceFeedback>();
	}

	private void Awake() {
		if(!teamMember)
			teamMember  = GetComponentInChildren<TeamMember>();
		
		if(!playerLabel)
			playerLabel = GetComponentInChildren<TMP_Text>();
	}

	public GameObject CreateDeadMarker() {
		if(forceFeedback)
			forceFeedback.Rumble(deadFeedbackLowFreq, deadFeedbackHighFreq, deadFeedbackDuration);
		
		if (!deadMarkerPrefab)
			return null;

		if (!cachedDeadMarker || lastTeam != teamMember.GetTeam()) {
			if(cachedDeadMarkerGo)
				Destroy(cachedDeadMarkerGo);
			
			cachedDeadMarkerGo = Instantiate(deadMarkerPrefab);
			cachedDeadMarker   = cachedDeadMarkerGo.GetComponent<DeadMarker>();
		
			cachedDeadMarker.teamMember.ChangeTeam(teamMember.GetTeam());
			cachedDeadMarker.playerLabel.text  = playerLabel.text;
			cachedDeadMarker.playerLabel.color = playerLabel.color;
			lastTeam                           = teamMember.GetTeam();
			Debug.Log("Recreating Dead-Marker for "+gameObject.name);
		}
		
		cachedDeadMarker.timer.remainingSeconds = respawnDelay + 0.5f;
		cachedDeadMarkerGo.SetActive(true);
		cachedDeadMarker.StartCoroutine(DisableAfter(cachedDeadMarkerGo, respawnDelay));

		return cachedDeadMarkerGo;
	}

	private static IEnumerator DisableAfter(GameObject go, float seconds) {
		yield return new WaitForSecondsRealtime(seconds);
		go.SetActive(false);
		go.transform.parent = null;
	}

}
