using System;
using UnityEngine;

namespace Items {
	public abstract class AbstractItemEffect : MonoBehaviour {
		private bool used = false;

		protected virtual bool IsSingleUse() {
			return true;
		}
		protected virtual bool IsSContinuous() {
			return false;
		}
		
		private void OnTriggerEnter2D(Collider2D other) {
			if (!used && other.TryGetComponent(out PlayerMovement p)) {
				if(IsSingleUse())
					used = true;
				
				OnUse(p.gameObject);
			}
		}

		private void OnTriggerStay2D(Collider2D other) {
			if (!IsSContinuous())
				return;

			OnTriggerEnter2D(other);
		}

		protected abstract void OnUse(GameObject player);
		
	}
}
