using UnityEngine;

namespace Items {
	public class AnvilItem : AbstractItemEffect {
		[SerializeField] private float downVelocity;

		protected override bool IsSingleUse() {
			return false;
		}

		protected override bool IsSContinuous() {
			return true;
		}

		protected override void OnUse(GameObject player) {
			var body = player.GetComponent<Rigidbody2D>();
			body.velocity = new Vector3(body.velocity.x, -downVelocity);
		}
	}
}
