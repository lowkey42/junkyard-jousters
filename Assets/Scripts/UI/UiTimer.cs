using DG.Tweening;
using TMPro;
using UnityEngine;

public class UiTimer : BeatAwareMonoBehaviour {
	[SerializeField] private TMP_Text    text;
	[SerializeField] private AudioSource music;

	private Tween fade;
	private bool  gameOver = false;

	protected override void OnEnable() {
		base.OnEnable();
		BeatManager.OnGameOver += OnGameOver;
	}
	protected override void OnDisable() {
		base.OnDisable();
		BeatManager.OnGameOver -= OnGameOver;
	}

	private void OnGameOver() {
		gameOver = true;
	}
	
	private void Update() {
		if (gameOver) {
			if (fade != null && fade.active)
				fade.Kill();
			
			text.color = Color.white;
			text.text  = "Game Over";
			return;
		}
		
		var remaining = music.clip.length - music.time;
		var minutes   = (int) (remaining / 60);
		var seconds   = ((int) remaining) % 60;
		text.text = $"{minutes}:{seconds:00}";
	}

	protected override void OnBeat(float timeToNextBeat) {
		if (fade != null && fade.active)
			fade.Kill();

		text.color = Color.white;
		fade       = text.DOColor(Color.gray, timeToNextBeat / 2f).SetUpdate(true);
	}
}
