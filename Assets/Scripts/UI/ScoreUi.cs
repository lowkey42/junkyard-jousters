using TMPro;
using UnityEngine;

public class ScoreUi : MonoBehaviour
{
	[SerializeField] private TMP_Text[] scoreTexts;
	
    void Update() {
	    for (int i = 0; i < scoreTexts.Length; i++)
		    scoreTexts[i].text = $"{Score.Instance.GetScore(i)}";
    }
}
