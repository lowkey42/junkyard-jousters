using System;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class Pooled : MonoBehaviour {
	public GameObjectPool pool;

	public void ReturnToPool() {
		pool.Return(gameObject);
	}
}

public class GameObjectPool : MonoBehaviour {
	[ShowAssetPreview] [SerializeField] private List<GameObject> prefabs;

	[SerializeField] private int targetSize;
	
	[SerializeField, HideInInspector] 
	private List<GameObject> instantiatedObjects = new();

	[ShowNativeProperty]
	public int UsedCapacity => targetSize-AvailableCapacity;

	[ShowNativeProperty]
	public int AvailableCapacity => instantiatedObjects.Count;
	
	private void Awake() {
		Populate();
	}

	public void Return(GameObject go) {
		go.SetActive(false);
		instantiatedObjects.Add(go);
	}

	public GameObject TakeInstance() {
		if (instantiatedObjects.Count == 0) {
			Populate();
		}

		var go = instantiatedObjects[instantiatedObjects.Count - 1];
		instantiatedObjects.RemoveAt(instantiatedObjects.Count - 1);
		go.SetActive(true);
		return go;
	}

	private void Populate() {
		if (instantiatedObjects.Count > 0)
			return;
		
		for (var i = 0; i < targetSize; ++i) {
			var prefabIndex = Random.Range(0, prefabs.Count);
			var prefab      = prefabs[prefabIndex];
			var go          = Instantiate(prefab, gameObject.transform);
			go.AddComponent<Pooled>().pool = this;
			go.SetActive(false);
			instantiatedObjects.Add(go);
		}

		Debug.Log($"Object pool \"{GetPath(transform)}\" populated with {targetSize} GameObjects");
	}

	private static string GetPath(Transform current) {
		if (!current.parent)
			return "/" + current.name;
		return GetPath(current.parent) + "/" + current.name;
	}
}
