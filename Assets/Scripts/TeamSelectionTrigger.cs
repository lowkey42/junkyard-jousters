using UnityEngine;

public class TeamSelectionTrigger : MonoBehaviour {
	public Team team;

	void OnTriggerEnter2D(Collider2D collider2D) {
		if(collider2D.TryGetComponent(out TeamMember tm))
			tm.ChangeTeam(team);
	}
}
