using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class LimitFallSpeed : MonoBehaviour {
	[SerializeField] private float maxFallSpeed = 40f;

	private Rigidbody2D body;

	private void Awake() {
		if (!body)
			body = GetComponent<Rigidbody2D>();
	}

	private void OnValidate() {
		body = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate() {
		var v = body.velocity;

		if (v.y < -maxFallSpeed) v.y = -maxFallSpeed;
		
		body.velocity = v;
	}
}
