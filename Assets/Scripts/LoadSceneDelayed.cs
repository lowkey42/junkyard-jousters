using System;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneDelayed : MonoBehaviour {
	[SerializeField, Scene] private string sceneName;

	[SerializeField] private float delay = 4f;

	private float delayOver;

	private void Start() {
		delayOver = Time.unscaledTime + delay;
	}

	private void Update() {
		if (delayOver <= Time.unscaledTime || Input.anyKey) {
			SceneManager.LoadScene(sceneName);
		}
	}
}
