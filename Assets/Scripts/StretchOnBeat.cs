using System;
using DG.Tweening;
using UnityEngine;

public class StretchOnBeat : BeatAwareMonoBehaviour {

	[SerializeField] private Vector2 scaleBeat = new Vector2(0.2f, 0.2f);
	
	protected override void OnBeat(float timeToNextBeat) {
		transform.DOPunchScale(scaleBeat, timeToNextBeat, 2, 0.1f).SetUpdate(true);
	}
}
