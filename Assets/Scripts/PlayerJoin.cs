using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllerAssignment {
	public PlayerJoin joiner;
	public bool       rightSide;
}

public class PlayerJoin : MonoBehaviour {
	[SerializeField, Scene] private string         lobbyScene = "TeamSelection";
	[SerializeField]        private AudioClip      joinNoise;
	[SerializeField]        private AudioClip      leaveNoise;
	[SerializeField]        private ParticleSystem joinParticleSystem;

	public  GameObject   player_left;
	private LobbyManager lm;
	private bool         initialized = false;
	public  GameObject   player_right;

	public int  playerCount = 0;
	public bool inLobby     = true;

	public void OnExit() {
		if (SceneManager.GetActiveScene().name == lobbyScene)
			Application.Quit();
		else
			SceneManager.LoadScene(lobbyScene, LoadSceneMode.Single);
	}

	public void OnNextTrack() {
		if(lm)
			lm.NextTrack();
	}
	public void OnPreviousTrack() {
		if(lm)
			lm.PreviousTrack();
	}

	public void OnJoin_Left() {
		Join(player_left, false);
	}

	public void OnJoin_Right() {
		Join(player_right, true);
	}


	void Start() {
		if (initialized)
			return;

		inLobby     = true;
		initialized = true;
		lm          = FindObjectOfType<LobbyManager>();
		player_left.SetActive(false);
		player_right.SetActive(false);
	}

	private void Join(GameObject parent, bool rightSide) {
		if (!inLobby)
			return;

		if (!initialized)
			Start();

		var position = new Vector3(Random.Range(-20, 20), Random.Range(5, 16));

		if (!parent.activeSelf) {
			// join
			var p = lm.AddPlayer();
			if (!p)
				return;
			
			playerCount++;
			parent.SetActive(true);
			
			p.transform.parent   = parent.transform;
			p.transform.position = position;

			p.SendMessage("AssignController", new ControllerAssignment(){joiner = this, rightSide =rightSide});

			AudioSource.PlayClipAtPoint(joinNoise, position, 1.0f);
			
		} else {
			// un-join
			var p = parent.transform.GetChild(0);
			
			p.SendMessage("UnAssignController", new ControllerAssignment(){joiner = this, rightSide =rightSide}, SendMessageOptions.DontRequireReceiver);
			
			playerCount--;
			position = p.position;

			lm.RemovePlayer(p.gameObject);

			parent.SetActive(false);
			AudioSource.PlayClipAtPoint(leaveNoise, position, 1.0f);
			Destroy(p.gameObject);
		}


		if (joinParticleSystem) {
			joinParticleSystem.gameObject.transform.position = position;
			joinParticleSystem.Stop();
			joinParticleSystem.Play();
		}
	}
}
