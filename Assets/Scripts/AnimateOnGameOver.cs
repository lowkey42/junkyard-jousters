using NaughtyAttributes;
using UnityEngine;

public class AnimateOnGameOver : MonoBehaviour {
	private new Animation animation;

	private void Start() {
		animation = GetComponent<Animation>();
	}

	private void OnEnable() {
		BeatManager.OnGameOver += OnGameOver;
	}

	private void OnDisable() {
		BeatManager.OnGameOver -= OnGameOver;
	}

	[Button("Trigger Animation")]
	private void OnGameOver() {
		if (animation) {
			animation.Stop();
			animation.Play("CameraAnimation");
		}
	}
}
