using System;
using DG.Tweening;
using UnityEngine;

public class AlphaOnBeat : BeatAwareMonoBehaviour {

	[SerializeField] private float alphaBeat = 0.4f;

	[SerializeField] private SpriteRenderer sprite;

	private void OnValidate() {
		sprite = GetComponent<SpriteRenderer>();
	}

	private void Awake() {
		if(!sprite)
			sprite = GetComponent<SpriteRenderer>();
	}

	protected override void OnBeat(float timeToNextBeat) {
		if (!sprite) return;
		
		var c        = sprite.color;
		var orgAlpha = c.a;
		c.a          += alphaBeat;
		sprite.color =  c;
		DOTween.To(v => {
			           var c = sprite.color;
			           c.a          = v;
			           sprite.color = c;
		           }, c.a, orgAlpha, timeToNextBeat / 2f)
		       .SetEase(Ease.InOutQuad)
		       .SetUpdate(true);
	}
}
