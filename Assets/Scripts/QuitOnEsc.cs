﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitOnEsc : MonoBehaviour
{
	[SerializeField, Scene]
	private string lobbyScene = "TeamSelection";
	
	void Update()
    {
        if (Input.GetKey ("escape")) {
	        if (SceneManager.GetActiveScene().name == lobbyScene)
		        Application.Quit();
	        else
		        SceneManager.LoadScene(lobbyScene, LoadSceneMode.Single);
        }
    }
}
