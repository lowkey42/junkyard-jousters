using System;
using UnityEngine;

public abstract class BeatAwareMonoBehaviour : MonoBehaviour {

	protected abstract void OnBeat(float timeToNextBeat); 
	
	protected virtual void OnEnable() {
		BeatManager.OnBeat += OnBeat;
	}
	protected virtual void OnDisable() {
		BeatManager.OnBeat -= OnBeat;
	}
}
