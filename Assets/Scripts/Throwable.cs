using System;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour {
	[SerializeField] private SpriteGlowRedirect glow;
	[SerializeField] private PlayerMovement     playerMovement;
	private                  HashSet<Color>     colors    = new();
	private                  Color              lastColor = Color.black;

	private void OnValidate() {
		playerMovement = GetComponent<PlayerMovement>();
	}

	private void OnEnable() {
		colors.Clear();
		lastColor = Color.black;
		if(glow)
			glow.enabled = false;
	}
	private void OnDisable() {
		colors.Clear();
		lastColor = Color.black;
		if(glow)
			glow.enabled = false;
	}

	public void OnThrown(Color color) {
		if(playerMovement)
			playerMovement.OnThrown();

		if (glow) {
			color.a = 1f;
			glow.SetColor(color);
		}
	}

	public void SetColor(Color color) {
		if (colors.Count == 0 && glow) {
			glow.SetColor(color);
			lastColor = color;
		}

		colors.Add(color);
	}

	public void ReplaceColor(Color oldColor, Color newColor) {
		if (colors.Remove(oldColor)) {
			colors.Add(newColor);
			if (lastColor == oldColor) {
				lastColor = newColor;
				if(glow)
					glow.SetColor(newColor);
			}
		}
	}

	public void UnsetColor(Color color) {
		if (colors.Remove(color) && colors.Count == 0) {
			if(glow)
				glow.enabled = false;
		}
	}

	private void Update() {
		if (glow && colors.Count == 0) {
			lastColor.a = Mathf.MoveTowards(lastColor.a, 0f, 2f * Time.unscaledDeltaTime);
			glow.SetColor(lastColor);
		}
	}
}
