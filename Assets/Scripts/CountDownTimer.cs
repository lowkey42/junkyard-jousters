using TMPro;
using UnityEngine;

namespace DefaultNamespace {
	public class CountDownTimer : MonoBehaviour {

		public float remainingSeconds = 0;

		[SerializeField] private TMP_Text label;

		private void Update() {
			if (remainingSeconds > 0) {
				label.text       =  $"{(int)remainingSeconds}";
				remainingSeconds -= Time.unscaledDeltaTime;
			}
		}
	}
}
