using System;
using UnityEngine;

public class LimitTrailRendererLength : MonoBehaviour {
	[SerializeField] private float         maxDistance = 4f;
	[SerializeField] private TrailRenderer trail;

	private Vector3 lastPosition;

	private void OnValidate() {
		trail = GetComponent<TrailRenderer>();
	}

	private void Awake() {
		if (!trail)
			trail = GetComponent<TrailRenderer>();

		lastPosition = transform.position;
	}

	private void Update() {
		var p = transform.position;
		if ((p - lastPosition).sqrMagnitude > maxDistance * maxDistance)
			trail.Clear();

		lastPosition = p;
	}
}
