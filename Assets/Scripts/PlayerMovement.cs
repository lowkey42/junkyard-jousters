using System;
using System.Collections;
using DG.Tweening;
using SpriteGlow;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public struct SquashSettings {
	[Tooltip("Faktor zum Verzerren in der Breite (<1 schmaler, >1 breiter)")]
	public float widthFactor;

	[Tooltip("Faktor zum Verzerren in der Höhe (<1 kleiner, >1 größer)")]
	public float heightFactor;

	[Tooltip("Sekunden bis wir zur ursprünglichen Breite/Höhe zurückkehren")]
	public float duration;

	public SquashSettings(float widthFactor, float heightFactor, float duration) {
		this.widthFactor  = widthFactor;
		this.heightFactor = heightFactor;
		this.duration     = duration;
	}
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : BeatAwareMonoBehaviour {
	private static readonly int Anim_walkSpeed = Animator.StringToHash("walkSpeed");
	private static readonly int Anim_falling   = Animator.StringToHash("falling");
	private static readonly int Anim_jump      = Animator.StringToHash("jump");
	private static readonly int Anim_colliding = Animator.StringToHash("colliding");


	[SerializeField] private float maxMovementSpeed   = 10f;
	[SerializeField] private float acceleration       = 50f;
	[SerializeField] private float deceleration       = 20f;
	[SerializeField] private float decelerationToStop = 50f;
	[SerializeField] private float turnSpeed          = 80f;
	[SerializeField] private float slowFallSpeed      = 10f;
	[SerializeField] private float fastFallSpeed      = 30f;
	[SerializeField] private float normalFallSpeed    = 20f;
	[SerializeField] private float fallAcceleration   = 100f;

	[SerializeField] private float jumpVelocity     = 20f;
	[SerializeField] private float jumpPushVelocity = 20f;
	[SerializeField] private float jumpBuffer       = 0.15f;
	[SerializeField] private bool  canAlwaysJump    = false;

	[SerializeField] private float maxLeanStanding = 15f;
	[SerializeField] private float leanSpeed       = 360f;

	[SerializeField] private float     groundedRaycastLength = 2f;
	[SerializeField] private LayerMask groundedRaycastLayers = ~0;

	[SerializeField] private SquashSettings jumpStretch = new SquashSettings(0.5f, 1.8f, 0.1f);
	[SerializeField] private SquashSettings landSquash  = new SquashSettings(1.5f, 0.4f, 0.2f);
	[SerializeField] public  float          landDrop    = 0.2f;

	[SerializeField] private AudioClip[] grabNoises;
	[SerializeField] private AudioClip   grabFailNoise;
	[SerializeField] private AudioClip   hardHitNoise;
	[SerializeField] private AudioClip   lightHitNoise;

	[SerializeField] private Rigidbody2D  body;
	[SerializeField] private JumpDetector jumpDetector;
	[SerializeField] private Animator     animator;
	[SerializeField] private Transform    sprite;

	[SerializeField] private GameObject       aimIndicator;
	[SerializeField] private SpriteGlowEffect aimIndicatorGlow;
	[SerializeField] private SpriteGlowEffect aimIndicatorError;
	[SerializeField] private float            aimIndicatorMinBrightness = 2f;
	[SerializeField] private float            aimIndicatorMaxBrightness = 10f;

	[SerializeField] private bool disableWalkAnimation = false;
	[SerializeField] private bool flipX = false;

	[SerializeField] private float thrownStun = 0.25f;

	[SerializeField] private float beatLatency = 0.02f;

	[SerializeField] private ParticleSystem jumpParticleSystem;


	[SerializeField] private float dashFailFeedbackLowFreq  = 0.5f;
	[SerializeField] private float dashFailFeedbackHighFreq = 0.5f;
	[SerializeField] private float dashFailFeedbackDuration = 0.4f;
	
	[SerializeField] private float thrownFeedbackLowFreq  = 0.7f;
	[SerializeField] private float thrownFeedbackHighFreq = 0.2f;
	[SerializeField] private float thrownFeedbackDuration = 0.2f;
	
	private ForceFeedback forceFeedback;
	
	private Coroutine     squashAndStretchCoroutine;

	private Vector2 direction     = Vector2.up;
	private Vector2 lastDirection = Vector2.up;
	private bool    jumpNow;
	private bool    jumpOnNextBeat;
	private bool    beat               = false;
	private float   leanAngle          = 0f;
	private float   thrownStunOverTime = 0f;
	private bool    alreadyJumped      = false;

	public void Direction(Vector2 newDirection) {
		direction = newDirection;
		if (newDirection.sqrMagnitude > 0.25f * 0.25f) {
			lastDirection = newDirection.normalized;
		}
	}

	public void Jump() {
		if (thrownStunOverTime > Time.unscaledTime) {
			OnJumpFailed();
			return;
		}

		var beatManager = BeatManager.Instance;
		jumpOnNextBeat = beatManager.TimeSinceBeat + beatLatency <= jumpBuffer;
		jumpNow        = !jumpOnNextBeat && !alreadyJumped && beatManager.TimeToNextBeat - beatLatency <= jumpBuffer;

		if (!jumpOnNextBeat && !jumpNow)
			OnJumpFailed();
	}

	public void OnThrown() {
		thrownStunOverTime = Time.unscaledTime + thrownStun;
		
		if(forceFeedback)
			forceFeedback.Rumble(thrownFeedbackLowFreq, thrownFeedbackHighFreq, thrownFeedbackDuration);
	}

	private void OnValidate() {
		body         = GetComponent<Rigidbody2D>();
		jumpDetector = GetComponentInChildren<JumpDetector>();
		animator     = GetComponentInChildren<Animator>();
		if (animator)
			sprite = animator.transform.parent;
	}

	private void Awake() {
		if (!body)
			body = GetComponent<Rigidbody2D>();

		if (!jumpDetector)
			jumpDetector = GetComponentInChildren<JumpDetector>();

		if (!animator) {
			animator = GetComponentInChildren<Animator>();
			sprite   = animator.transform.parent;
		}
	}

	public void AssignController(ControllerAssignment a) {
		forceFeedback = GetComponentInParent<ForceFeedback>();
	}
	
	protected override void OnBeat(float timeToNextBeat) {
		beat          = true;
		alreadyJumped = false;

		aimIndicatorGlow.GlowBrightness = aimIndicatorMaxBrightness;
		DOTween.To(v => aimIndicatorGlow.GlowBrightness = v, aimIndicatorMaxBrightness, aimIndicatorMinBrightness, timeToNextBeat / 2f)
		       .SetEase(Ease.InOutQuad)
		       .SetUpdate(true);
	}

	private void Update() {
		if (animator) {
			var falling = !Physics2D.Raycast(transform.position, Vector2.down, groundedRaycastLength, groundedRaycastLayers);
			animator.SetBool(Anim_falling, falling);

			var speed = body.velocity.x;
			animator.SetFloat(Anim_walkSpeed, disableWalkAnimation ? 0 : Mathf.Abs(speed/10f));

			// flip the character to look in movement direction
			var scale = animator.transform.localScale;
			if (flipX) {
				scale.x = Mathf.Abs(scale.x) * (speed < 0f ? 1f : -1f);
			} else {
				scale.x = Mathf.Abs(scale.x) * (speed < 0f ? -1f : 1f);
			}
			animator.transform.localScale = scale;

			// orient player toward jump direction
			var angle = Angle(direction);
			angle           = Mathf.Clamp(angle                             / 2f, -maxLeanStanding, maxLeanStanding);
			leanAngle       = Mathf.MoveTowards(leanAngle, angle, leanSpeed * Time.deltaTime);
			sprite.rotation = Quaternion.AngleAxis(leanAngle, Vector3.forward);
		}

		if (aimIndicator && aimIndicatorGlow) {
			aimIndicatorGlow.enabled        = jumpDetector.IsAny() || canAlwaysJump;
			aimIndicator.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(lastDirection.y, lastDirection.x) * Mathf.Rad2Deg, Vector3.forward);
		}
	}

	private static float Angle(Vector2 dir) {
		if (dir.sqrMagnitude < 0.1f)
			return 0;

		dir = dir.normalized;
		var angle               = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;
		if (angle < -180) angle += 360;
		if (angle > 180) angle  -= 360;
		return angle;
	}

	private void OnCollisionEnter2D(Collision2D col) {
		if (!gameObject.activeInHierarchy)
			return;
		
		if (Mathf.Abs(col.relativeVelocity.x) > maxMovementSpeed || Mathf.Abs(col.relativeVelocity.y) > normalFallSpeed / 2f) {
			StartSquashAndStretch(landSquash, landDrop);
			//AudioSource.PlayClipAtPoint(lightHitNoise, new Vector3(body.position.x, body.position.y), 1.0f);
		}

		if (animator && (Mathf.Abs(col.relativeVelocity.x) > maxMovementSpeed * 1.5f || Mathf.Abs(col.relativeVelocity.y) > normalFallSpeed)) {
			animator.SetTrigger(Anim_colliding);
			AudioSource.PlayClipAtPoint(hardHitNoise, new Vector3(body.position.x, body.position.y), 1.0f);
		}
	}

	private void FixedUpdate() {
		if (thrownStunOverTime > Time.unscaledTime)
			return;
		if (!gameObject.activeInHierarchy)
			return;

		DoMove();

		if (jumpNow || (jumpOnNextBeat && beat)) {
			if (jumpOnNextBeat && beat)
				alreadyJumped = true;

			jumpNow        = false;
			jumpOnNextBeat = false;
			beat           = false;
			DoJump();
		}
	}

	private void DoMove() {
		var velocity       = body.velocity;
		var targetVelocity = direction.x * maxMovementSpeed;

		var accel = Mathf.Abs(velocity.x) > maxMovementSpeed && Mathf.Abs(direction.x) > 0.5f ? deceleration : decelerationToStop;
		if ((int) Mathf.Sign(targetVelocity) != (int) Mathf.Sign(velocity.x)) {
			accel = turnSpeed;
		} else if (Mathf.Abs(targetVelocity) > Mathf.Abs(velocity.x)) {
			accel = acceleration;
		}

		velocity.x = Mathf.MoveTowards(velocity.x, targetVelocity, accel * Time.fixedDeltaTime);

		if (Mathf.Abs(direction.y) > 0.5f) {
			var targetFallSpeed = direction.y > 0f ? slowFallSpeed : fastFallSpeed;
			if (velocity.y < -targetFallSpeed || direction.y < 0)
				velocity.y = Mathf.MoveTowards(velocity.y, -targetFallSpeed, fallAcceleration * Time.fixedDeltaTime);
		} else {
			velocity.y = Mathf.Max(velocity.y, -normalFallSpeed);
		}

		body.velocity = velocity;
	}

	private void DoJump() {
		if (!jumpDetector.IsAny() && !canAlwaysJump) {
			if (animator)
				animator.SetTrigger(Anim_colliding);

			OnJumpFailed();

			return;
		}

		if (jumpParticleSystem) {
			jumpParticleSystem.Stop();
			jumpParticleSystem.Play();
		}

		body.velocity = lastDirection * jumpVelocity;
		jumpDetector.PushAll(-lastDirection * jumpPushVelocity);
		if (animator) {
			int audioIndex = Random.Range(0, grabNoises.Length);
			AudioSource.PlayClipAtPoint(grabNoises[audioIndex], new Vector3(body.position.x, body.position.y), 1.0f);
			animator.SetTrigger(Anim_jump);
		}

		leanAngle       = Angle(lastDirection);
		sprite.rotation = Quaternion.AngleAxis(leanAngle, Vector3.forward);

		StartSquashAndStretch(jumpStretch, 0f);
	}

	private float lastFailedJumpIndication = 0;

	private void OnJumpFailed() {
		if (lastFailedJumpIndication + 1f > Time.unscaledTime)
			return;

		lastFailedJumpIndication = Time.unscaledTime;

		AudioSource.PlayClipAtPoint(grabFailNoise, new Vector3(body.position.x, body.position.y), 1.0f);

		if (aimIndicatorError) {
			aimIndicatorError.gameObject.SetActive(true);
			DOTween.To(v => aimIndicatorError.GlowBrightness = v, 2f, 10f, 0.05f)
			       .SetEase(Ease.InOutQuad)
			       .SetLoops(8, LoopType.Yoyo)
			       .SetUpdate(true)
			       .OnComplete(() => aimIndicatorError.gameObject.SetActive(false));
		}
		
		if(forceFeedback)
			forceFeedback.Rumble(dashFailFeedbackLowFreq, dashFailFeedbackHighFreq, dashFailFeedbackDuration);
	}


	private void StartSquashAndStretch(SquashSettings settings, float drop) {
		if (squashAndStretchCoroutine != null) StopCoroutine(squashAndStretchCoroutine);
		squashAndStretchCoroutine = StartCoroutine(SquashAndStretch(settings.widthFactor, settings.heightFactor,
		                                                            settings.duration,    drop));
	}

	private IEnumerator SquashAndStretch(float xFactor,
	                                     float yFactor,
	                                     float duration,
	                                     float drop) {
		var newSize = new Vector3(xFactor, yFactor, 1f);
		var newPos  = new Vector3(0,       -drop,   0);

		// Zuerst verzerren wir das Sprite schnell (in 0.1 Sekunden) auf die gewünschten Maße
		for (var t = 0f; t <= 1f; t += Time.deltaTime / 0.1f) {
			sprite.localScale    = Vector3.Lerp(Vector3.one,  newSize, t);
			sprite.localPosition = Vector3.Lerp(Vector3.zero, newPos,  t);
			yield return null; // (nach jeder Änderung der Maße warten wir bis zum nächsten Frame)
		}

		sprite.localScale    = newSize;
		sprite.localPosition = newPos;

		// ... und danach strecken wir es langsam, über die festgelegte Dauer, wieder auf seine ursprünglichen Maße
		for (var t = 0f; t <= 1f; t += Time.deltaTime / duration) {
			sprite.localScale    = Vector3.Lerp(newSize, Vector3.one,  t);
			sprite.localPosition = Vector3.Lerp(newPos,  Vector3.zero, t);
			yield return null; // (nach jeder Änderung der Maße warten wir bis zum nächsten Frame)
		}

		sprite.localScale    = Vector3.one;
		sprite.localPosition = Vector3.zero;

		squashAndStretchCoroutine = null;
	}
}
