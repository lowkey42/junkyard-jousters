using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class Spawn {
	[ShowAssetPreview] public GameObject prefab;

	public float spawnProbability = 1f;

	public int maxSpawns = -1;
}

public class Spawner : BeatAwareMonoBehaviour {
	[MinMaxSlider(0.1f, 20f)] [SerializeField]
	private Vector2 timeBetweenBursts = new Vector2(2f, 10f);

	[MinMaxSlider(1f, 100f)] [SerializeField]
	private Vector2 burstSize = new Vector2(5f, 15f);

	[SerializeField] private float itemProbability = 0.5f;

	[SerializeField] private float width = 5f;

	[SerializeField] private float height = 5f;

	[SerializeField] private float directionAngle = 45f;

	[SerializeField] private float force = 100f;

	[SerializeField] private float maxAngularVelocity = 50f;

	[SerializeField] private List<Spawn> spawnsItems;

	[SerializeField] private GameObjectPool garbagePool;
	
	[SerializeField] private AudioClip garbageSpawnNoise;
	[SerializeField] private AudioClip itemSpawnNoise;

	[SerializeField] private ParticleSystem burstParticles;


	private List<GameObject> spawnNext = new();

	private float nextBurst = 0;
	private bool  gameOver = false;

	protected override void OnEnable() {
		base.OnEnable();
		BeatManager.OnGameOver += OnGameOver;
	}
	protected override void OnDisable() {
		base.OnDisable();
		BeatManager.OnGameOver -= OnGameOver;
	}

	private void OnGameOver() {
		gameOver = true;
	}
	
	public void QueueRespawn(GameObject go, float delay) {
		if (gameOver)
			return;
		
		go.SetActive(false);

		var nextBurstIn = nextBurst - Time.unscaledTime;

		if (Mathf.Abs(nextBurstIn - delay) < 2f) {
			spawnNext.Add(go);
			nextBurst = Time.unscaledTime + delay;
		} else if (nextBurstIn < delay) {
			// delay would be too short => retry later
			StartCoroutine(QueueSpawnIn(go, delay));
		}  else {
			// delay would be too long => spawn outside of burst
			StartCoroutine(SpawnIn(go, delay));
		}
	}

	private IEnumerator QueueSpawnIn(GameObject go, float delay) {
		yield return new WaitForSecondsRealtime(delay);
		QueueRespawn(go, 0);
	}
	private IEnumerator SpawnIn(GameObject go, float delay) {
		yield return new WaitForSecondsRealtime(delay);
		Spawn(go);
		BurstEffects();
	}

	private void Start() {
		nextBurst = Time.unscaledTime + Random.Range(timeBetweenBursts.x, timeBetweenBursts.y);
	}

	protected override void OnBeat(float _) {
		if (Time.unscaledTime >= nextBurst) {
			nextBurst = Time.unscaledTime + Random.Range(timeBetweenBursts.x, timeBetweenBursts.y);

			// Spawn queued objects (mostly player respawns)
			spawnNext.ForEach(Spawn);

			if (spawnNext.Count == 0 && spawnsItems.Count>0 && Random.Range(0f, 1f) <= itemProbability) {
				var itemIndex = Random.Range(0, spawnsItems.Count);
				var item      = spawnsItems[itemIndex];
				Spawn(Instantiate(item.prefab));
				AudioSource.PlayClipAtPoint(itemSpawnNoise, transform.position, 1.0f);

				if (item.maxSpawns > 0) {
					item.maxSpawns--;
					if (item.maxSpawns <= 0) {
						spawnsItems.RemoveAt(itemIndex);
					}
				}
			}
			
			// pick objects to spawn
			var count = Random.Range(burstSize.x, burstSize.y);
			for(int i=0; i < count; ++i) {
				Spawn(garbagePool.TakeInstance());
			}

			if (count > 2 || spawnNext.Count>0) {
				BurstEffects();
			}

			// spawn all objects
			spawnNext.Clear();
		}
	}

	private void BurstEffects() {
		AudioSource.PlayClipAtPoint(garbageSpawnNoise, transform.position, 1.0f);

		if (burstParticles && burstParticles.gameObject.activeSelf) {
			burstParticles.Stop();
			burstParticles.Play();
		}
	}

	private void Spawn(GameObject go) {
		var spawnerT = transform;

		go.SetActive(true);
		var t = go.transform;
		t.position = spawnerT.TransformPoint(new Vector3(Random.Range(-width / 2f, width / 2f), Random.Range(-height / 2f, height / 2f), 0f));

		var dir = spawnerT.TransformDirection(Quaternion.Euler(0, 0, Random.Range(-directionAngle / 2f, directionAngle / 2f)) * Vector3.down);

		var randomRotation = true;
		if (go.TryGetComponent(out Rigidbody2D body)) {
			randomRotation = !body.constraints.HasFlag(RigidbodyConstraints2D.FreezeRotation);

			body.AddForce(dir * force, ForceMode2D.Impulse);
			if (randomRotation)
				body.angularVelocity = Random.Range(-maxAngularVelocity, maxAngularVelocity);
		}

		if (randomRotation)
			t.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
	}

	private void OnDrawGizmos() {
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.color  = Color.green;
		Gizmos.DrawCube(Vector3.zero, new Vector3(width, height, 1f));
		Gizmos.color = Color.grey;
		Gizmos.DrawCube(new Vector3(0, height - 0.5f, 0f), new Vector3(width + 1f, 1f, 1f));
	}
}
