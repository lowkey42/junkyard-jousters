using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class Despawner : MonoBehaviour {
	private static readonly int Anim_dead = Animator.StringToHash("dead");

	[SerializeField]         private Spawner respawnTeamRed;
	[SerializeField]         private Spawner respawnTeamBlue;
	[Layer] [SerializeField] private int     deadPlayerLayer;
	[SerializeField]         private float   destroyPercentage = 0.5f;

	[SerializeField] private List<Transform> deadMarkerPositionsRed  = new();
	[SerializeField] private List<Transform> deadMarkerPositionsBlue = new();

	private bool gameOver = false;

	private void OnEnable() {
		BeatManager.OnGameOver += OnGameOver;
	}
	private void OnDisable() {
		BeatManager.OnGameOver -= OnGameOver;
	}

	private void OnGameOver() {
		gameOver = true;
	}
	
	private void OnTriggerEnter2D(Collider2D col) {
		// Player => remove controller => spawn new player after cooldown
		if (col.gameObject.TryGetComponent(out Respawn player))
			Kill(player);
		else if (col.attachedRigidbody) {
			if (Random.Range(0f, 1f) <= destroyPercentage) {
				// either return the object to its pool or destroy it
				var go = col.attachedRigidbody.gameObject;
				if (go.TryGetComponent(out Pooled pooled))
					pooled.ReturnToPool();
				else
					Destroy(col.attachedRigidbody.gameObject);
			} else {
				// mark objects to become non-interactive once they come to rest
				col.gameObject.AddComponent<DisableRigidBodyAtRest>().body = col.attachedRigidbody;
			}
		}
	}

	public void Kill(Respawn player) {
		SpawnDeadMarker(player);

		var go = player.gameObject;

		if (!gameOver) {
			var spawner = respawnTeamRed;

			if (player.TryGetComponent(out TeamMember tm)) {
				Score.Instance.IncrementOtherTeamScores(tm.GetTeam());

				if (tm.GetTeam() == Team.Blue) {
					spawner = respawnTeamBlue;
				}
			}

			if (spawner) {
				var clone = GameObject.Instantiate(go);
				go.SetActive(false);
				go.transform.position = new Vector3(0, 9999, 0);
				spawner.QueueRespawn(go, player.respawnDelay);
				go = clone; // keep the original and destroy the clone
			} else {
				Debug.LogWarning("No respawn points set");
			}
		}

		go.layer = deadPlayerLayer;

		if (go.TryGetComponent(out Respawn r))
			Destroy(r);
		if (go.TryGetComponent(out PlayerController pc))
			Destroy(pc);
		if (go.TryGetComponent(out PlayerInput pi))
			Destroy(pi);
		if (go.TryGetComponent(out PlayerMovement pm))
			Destroy(pm);
		if (go.TryGetComponent(out Animator a))
			a.SetBool(Anim_dead, true);

		// TODO: remove any additional controller or gameplay scripts

		if (go.TryGetComponent(out Rigidbody2D body)) {
			body.constraints                               = RigidbodyConstraints2D.None;
			go.AddComponent<DisableRigidBodyAtRest>().body = body;
		} else {
			Debug.LogWarning("Player without Rigidbody2D");
		}
	}

	private void SpawnDeadMarker(Respawn respawn) {
		var team = respawn.GetComponent<TeamMember>();
		if (!team)
			return;

		var positions = team.GetTeam() == Team.Blue ? deadMarkerPositionsBlue : deadMarkerPositionsRed;

		foreach (var p in positions) {
			if (p.childCount == 0) {
				var marker = respawn.CreateDeadMarker();
				marker.transform.SetParent(p, false);
				marker.transform.localPosition = Vector3.zero;
				return;
			}
		}
	}
}

public class DisableRigidBodyAtRest : MonoBehaviour {
	public Rigidbody2D body;

	private int atRestFor = 0;

	private void FixedUpdate() {
		if (!body) {
			Destroy(this);
			return;
		}

		if (body.velocity.sqrMagnitude > 0.1f) {
			atRestFor = 0;
			return;
		}

		atRestFor++;
		if (atRestFor > 10) {
			if (body.gameObject.TryGetComponent(out LimitFallSpeed lfs))
				Destroy(lfs);

			Destroy(body);
			Destroy(this);
		}
	}
}
