using System;
using System.Collections;
using System.Collections.Generic;
using SpriteGlow;
using TMPro;
using UnityEngine;

public enum Team {
	None = -1,
	Red  = 0,
	Blue = 1
}

public class TeamMember : MonoBehaviour {
	private static readonly  int  Anim_joined = Animator.StringToHash("joined");
	
	[SerializeField] private Team team           = Team.None;

	[SerializeField] private Color                  teamColorNeutral;
	[SerializeField] private Color                  teamColorNeutralAlt;
	[SerializeField] private Color                  teamColorRed;
	[SerializeField] private Color                  teamColorRedAlt;
	[SerializeField] private Color                  teamColorBlue;
	[SerializeField] private Color                  teamColorBlueAlt;
	[SerializeField] private List<SpriteGlowEffect> additionalGlows = new();

	[SerializeField] private SpriteRenderer influenceSprite;
	[SerializeField] private ParticleSystem influenceParticles;
	[SerializeField] private TrailRenderer  trail;

	[SerializeField] private TMP_Text label;

	[SerializeField] private AudioClip[] teamChangeNoises;
	
	[SerializeField] private ParticleSystem joinParticleSystem;

	[SerializeField] private SpriteSwap   spriteSwap;
	[SerializeField] private JumpDetector jumpDetector;
	[SerializeField] private Animator     animator;

	private Color TeamColor(Team t) {
		switch (t) {
			case Team.Red:  return teamColorRed;
			case Team.Blue: return teamColorBlue;
			default:
			case Team.None: return teamColorNeutral;
		}
	}

	private Color TeamColorAlt(Team t) {
		switch (t) {
			case Team.Red:  return teamColorRedAlt;
			case Team.Blue: return teamColorBlueAlt;
			default:
			case Team.None: return teamColorNeutralAlt;
		}
	}

	private void OnValidate() {
		spriteSwap   = GetComponentInChildren<SpriteSwap>();
		jumpDetector = GetComponentInChildren<JumpDetector>();
		animator     = GetComponentInChildren<Animator>();
	}

	private void Awake() {
		if(!spriteSwap)
			spriteSwap   = GetComponentInChildren<SpriteSwap>();
		
		if(!jumpDetector)
			jumpDetector = GetComponentInChildren<JumpDetector>();
		
		if(animator)
			animator = GetComponentInChildren<Animator>();
		
		ForceChangeTeam(team);
	}

	public void ChangeTeam(Team newTeam) {
		if (team == newTeam)
			return;

		ForceChangeTeam(newTeam);
	}

	private void ForceChangeTeam(Team newTeam) {
		team = newTeam;

		if (spriteSwap)
			spriteSwap.SwapSpritesTo(newTeam);

		if (jumpDetector)
			jumpDetector.ChangeColor(TeamColor(newTeam));

		if (label) {
			var c = TeamColor(newTeam);
			c.a         = label.color.a;
			c           = Color.Lerp(c, Color.white, 0.2f);
			label.color = c;
		}

		if (additionalGlows != null) {
			foreach (var glow in additionalGlows) {
				glow.GlowColor = TeamColor(newTeam);
			}
		}

		if (influenceSprite) {
			var c = TeamColor(newTeam);
			c                     = Color.Lerp(c, Color.white, 0.4f);
			c.a                   = influenceSprite.color.a;
			influenceSprite.color = c;
		}

		if (influenceParticles) {
			var m = influenceParticles.main;
			var sc = m.startColor;
			var c = TeamColor(team);
			c.a          = sc.colorMin.a;
			sc.colorMin  = c;
			
			c            = TeamColorAlt(team);
			c.a          = sc.colorMax.a;
			sc.colorMax  = c;
			m.startColor = sc;
		}

		if (trail) {
			var c = TeamColor(team);
			c.a              = trail.startColor.a;
			trail.startColor = c;
			
			c.a            = trail.endColor.a;
			trail.endColor = c;
		}

		animator.SetTrigger(Anim_joined);
		
		PlayTeamSound();

		if (joinParticleSystem && team != Team.None) {
			var m  = joinParticleSystem.main;
			var sc = m.startColor;
			var c  = TeamColor(team);
			c.a         = sc.colorMin.a;
			sc.colorMin = c;
			
			c            = TeamColorAlt(team);
			c.a          = sc.colorMax.a;
			sc.colorMax  = c;
			m.startColor = sc;
			joinParticleSystem.Stop();
			joinParticleSystem.Play();
			StartCoroutine(StopParticleSystem(joinParticleSystem, 1f));
		}
	}

	private IEnumerator StopParticleSystem(ParticleSystem ps, float delay) {
		yield return new WaitForSecondsRealtime(delay);
		ps.Stop();
	}

	private void PlayTeamSound() {
		if (teamChangeNoises.Length < 3)
			return;

		switch (team) {
			case Team.Blue:
				AudioSource.PlayClipAtPoint(teamChangeNoises[0], new Vector3(0, 0, 0), 1.0f);
				break;
			case Team.Red:
				AudioSource.PlayClipAtPoint(teamChangeNoises[1], new Vector3(0, 0, 0), 1.0f);
				break;
			case Team.None:
				AudioSource.PlayClipAtPoint(teamChangeNoises[2], new Vector3(0, 0, 0), 1.0f);
				break;
		}
	}

	public bool HasTeam() {
		return team != Team.None;
	}

	public Team GetTeam() {
		return team;
	}
}
