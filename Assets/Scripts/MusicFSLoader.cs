using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using UnityEngine;
using UnityEngine.Networking;

public class ExternalMusic {
	public string      title;
	public AudioClip   clip;
	public List<float> beats;
}

public static class MusicFSLoader {

	public static ExternalMusic MainTrack = null;
	
	public static List<ExternalMusic> GetTrackList(List<AudioClip> buildInMusic) {
		var list = new List<ExternalMusic>(ListExternalMusic());
		foreach (var m in buildInMusic) {
			var beats = BeatManager.LoadCsvByName(m.name, 0.1f);
			if (beats.Count == 0)
				continue;

			list.Add(new ExternalMusic() {
				                             title = m.name,
				                             clip  = m,
				                             beats = beats
			                             });
		}

		return list;
	}

	public static List<ExternalMusic> ListExternalMusic() {
#if UNITY_WEBGL
		return new List<ExternalMusic>();
#else
		if (_listExternalMusicCache != null)
			return _listExternalMusicCache;

		var loaded = new List<ExternalMusic>();

		var dir = new DirectoryInfo(Application.dataPath + "/..");
		foreach (var f in dir.GetFiles("*.mp3")) {
			Load(f, AudioType.MPEG, loaded);
		}

		foreach (var f in dir.GetFiles("*.ogg")) {
			Load(f, AudioType.OGGVORBIS, loaded);
		}

		foreach (var f in dir.GetFiles("*.wav")) {
			Load(f, AudioType.WAV, loaded);
		}

		if (loaded.Count > 0) {
			Debug.Log("Found " + loaded.Count + " external music files. Using first one.");
		} else {
			Debug.Log("No external music files found in " + Application.dataPath + "/..");
		}

		return _listExternalMusicCache = loaded;
#endif
	}

#if !UNITY_WEBGL
	private static List<ExternalMusic> _listExternalMusicCache = null;

	private static void Load(FileInfo file, AudioType type, List<ExternalMusic> output) {
		var audio = LoadAudio(file.FullName, type);
		if (!audio)
			return;

		var beatFile = Path.ChangeExtension(file.FullName, ".txt");
		var beatInfo = LoadText(beatFile);
		if (beatInfo == null)
			return;

		var beats = BeatManager.LoadCsv(beatInfo, 0.1f);

		output.Add(new ExternalMusic() {
			                               title = file.Name,
			                               clip  = audio,
			                               beats = beats
		                               });
	}

	private static AudioClip LoadAudio(string path, AudioType type) {
		using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(ToUrl(path), type)) {
			var req = www.SendWebRequest();

			while (!req.isDone) {
				Thread.Sleep(10);
			}

			if (www.result == UnityWebRequest.Result.ConnectionError) {
				Debug.Log("Failed to load audio assets from\"" + path + "\": " + www.error);
			} else {
				return DownloadHandlerAudioClip.GetContent(www);
			}
		}

		return null;
	}

	private static string LoadText(string path) {
		using (UnityWebRequest www = UnityWebRequest.Get(ToUrl(path))) {
			var req = www.SendWebRequest();

			while (!req.isDone) {
				Thread.Sleep(10);
			}

			if (www.result == UnityWebRequest.Result.ConnectionError) {
				Debug.Log("Failed to load audio assets from\"" + path + "\": " + www.error);
			} else {
				return www.downloadHandler.text;
			}
		}

		return null;
	}

	private static string ToUrl(string path) {
		return "file:///" +HttpUtility.UrlEncode(path.Replace(" ", "%20"));
	}
#endif
}
