using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerController : MonoBehaviour {
	private PlayerMovement movement;
	private bool           controllerSide; // False = left, True = right
	private PlayerJoin     join;
	
	private void Awake() {
		movement = GetComponent<PlayerMovement>();
	}

	public void AssignController(ControllerAssignment a) {
		join           = a.joiner;
		controllerSide = a.rightSide;
	}

	// Just add *some* kind of movement. The specifics here are not of interest. Serves just to
    // demonstrate that the inputs are indeed separate.
    public void OnGrab_Left()
    {
	    if (controllerSide == false || (join && join.playerCount<2))
	    {
		    if (movement)
			    movement.Jump();

		    //print("Grab Left");
	    }
    }

    public void OnGrab_Right()
    {
	    if (controllerSide || (join && join.playerCount <2))
	    {
		    if (movement)
			    movement.Jump();

		    //print("Grab Left");
	    }
    }

    public void OnMovement_Left(InputValue  direction)
    {
	    if (controllerSide == false || (join && join.playerCount <2))
	    {
		    if (movement)
			    movement.Direction(direction.Get<Vector2>());

		    //print("Move Left");
	    }
    }

    public void OnMovement_Right(InputValue direction)
    {
	    if (controllerSide || (join && join.playerCount <2))
	    {
		    if (movement)
			    movement.Direction(direction.Get<Vector2>());

		    //print("Move Left");
	    }
    }

}
