using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingAnimator : BeatAwareMonoBehaviour {

	[SerializeField] private float minChromaticAberration = 0f;
	[SerializeField] private float maxChromaticAberration = 0.2f;
	
	private ChromaticAberration chromaticAberration;
	private float               time = 0.1f;

	private void Start() {
		var volume = GetComponent<PostProcessVolume>();
		if(volume)
			chromaticAberration = volume.profile.GetSetting<ChromaticAberration>();
	}

	protected override void OnBeat(float timeToNextBeat) {
		chromaticAberration.intensity.value = maxChromaticAberration;
		time = timeToNextBeat;
	}

	private void Update() {
		var speed = (maxChromaticAberration-minChromaticAberration) / time * 2f;
		chromaticAberration.intensity.value = Mathf.MoveTowards(chromaticAberration.intensity.value, minChromaticAberration, speed * Time.unscaledDeltaTime);
	}
}
