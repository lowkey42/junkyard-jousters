using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class JumpDetector : MonoBehaviour {
	private Color playerColor;

	private readonly Dictionary<Collider2D, Throwable> inside = new();

	public void ChangeColor(Color color) {
		// update color in connected Throwables
		foreach (var c in inside) {
			if (c.Value)
				c.Value.ReplaceColor(playerColor, color);
		}
		
		playerColor = color;
	}
	
	private void OnEnable() {
		inside.Clear();
	}
	private void OnDisable() {
		inside.Clear();
	}

	public bool IsAny() {
		return inside.Count > 0;
	}

	public void PushAll(Vector2 newVelocity) {
		foreach (var c in inside) {
			if (c.Key && c.Key.attachedRigidbody) {
				c.Key.attachedRigidbody.velocity = newVelocity;

				if (c.Value)
					c.Value.OnThrown(playerColor);
			}
		}
	}

	private void Update() {
		var outdated = inside.Keys.Where(c => !c || !c.gameObject).ToArray();
		foreach (var k in outdated)
			inside.Remove(k);
	}

	private void OnTriggerEnter2D(Collider2D col) {
		if (gameObject.transform.IsChildOf(col.gameObject.transform))
			return;
		
		if (col.TryGetComponent(out Throwable t))
			t.SetColor(playerColor);
		
		inside.TryAdd(col, t);
	}

	private void OnTriggerExit2D(Collider2D col) {
		if (gameObject.transform.IsChildOf(col.gameObject.transform))
			return;

		if (inside.TryGetValue(col, out Throwable t)) {
			if(t)
				t.UnsetColor(playerColor);
			
			inside.Remove(col);
		}
	}
}
