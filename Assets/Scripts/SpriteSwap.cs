using System;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEngine;

[Serializable]
public class SpriteAlternatives {
	[ShowAssetPreview]
	public Sprite neural;
	[ShowAssetPreview]
	public Sprite red;
	[ShowAssetPreview]
	public Sprite blue;

	public bool Contains(Sprite s) {
		return s == neural || s == red || s == blue;
	}

	public Sprite Get(Team t) {
		switch (t) {
			case Team.Red:  return red;
			case Team.Blue: return blue;
			default:
			case Team.None: return neural;
		}
	}
}

public class SpriteSwap : MonoBehaviour {

	[SerializeField]
	private List<SpriteAlternatives> sprites = new();

	public void SwapSpritesTo(Team newTeam) {
		foreach (var sprite in GetComponentsInChildren<SpriteRenderer>()) {
			var entry = sprites.FirstOrDefault(s => s.Contains(sprite.sprite));
			if (entry!=null) {
				sprite.sprite = entry.Get(newTeam);
			}
		}
	}
	
}
