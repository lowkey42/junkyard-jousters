using System.Collections.Generic;
using SpriteGlow;
using UnityEngine;

public class SpriteGlowRedirect : MonoBehaviour {

	[SerializeField] private List<SpriteGlowEffect> glows = new();
	
	public void SetColor(Color c) {
		foreach (var g in glows) {
			g.GlowColor = c;
			g.enabled = c.a > 0f;
		}
	}
	
}
